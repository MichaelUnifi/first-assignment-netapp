

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/flow-monitor-helper.h"
/*

          Network Topology

     n0---p2p---n1 (10.1.1.0/24)

     n2---p2p---n3 (10.2.2.0/24)


*/



using namespace ns3;

NS_LOG_COMPONENT_DEFINE("FirstScriptExample");

int
main(int argc, char* argv[])
{
    CommandLine cmd(__FILE__);
    cmd.Parse(argc, argv);

    Time::SetResolution(Time::NS);

    LogComponentEnable("FlowMonitor", LOG_LEVEL_INFO);
    

    NodeContainer nodes01;
    nodes01.Create(2);

    NodeContainer nodes23;
    nodes23.Create(2);

    PointToPointHelper pointToPoint;
    pointToPoint.SetDeviceAttribute("DataRate", StringValue("5Mbps"));
    pointToPoint.SetChannelAttribute("Delay", StringValue("2ms"));

    

    NetDeviceContainer devices01;
    devices01 = pointToPoint.Install(nodes01);

    NetDeviceContainer devices23;
    devices23 = pointToPoint.Install(nodes23);

    InternetStackHelper stack;
    stack.Install(nodes01);
    stack.Install(nodes23);

    Ipv4AddressHelper address;
    address.SetBase("10.1.1.0", "255.255.255.0");

    Ipv4InterfaceContainer interfaces01 = address.Assign(devices01);

    address.SetBase("10.2.2.0", "255.255.255.0");

    Ipv4InterfaceContainer interfaces23 = address.Assign(devices23);


    UdpEchoServerHelper echoServer(9);

    ApplicationContainer serverApps01 = echoServer.Install(nodes01.Get(1));
    serverApps01.Start(Seconds(1.0));
    serverApps01.Stop(Seconds(10.0));

    ApplicationContainer serverApps23 = echoServer.Install(nodes23.Get(1));
    serverApps23.Start(Seconds(1.0));
    serverApps23.Stop(Seconds(10.0));

    UdpEchoClientHelper echoClient01(interfaces01.GetAddress(1), 9);
    echoClient01.SetAttribute("MaxPackets", UintegerValue(1000));
    echoClient01.SetAttribute("Interval", TimeValue(Seconds(0.001)));
    echoClient01.SetAttribute("PacketSize", UintegerValue(1024));

    ApplicationContainer clientApps01 = echoClient01.Install(nodes01.Get(0));
    clientApps01.Start(Seconds(2.0));
    clientApps01.Stop(Seconds(10.0));


    UdpEchoClientHelper echoClient23(interfaces23.GetAddress(1), 9);
    echoClient23.SetAttribute("MaxPackets", UintegerValue(1000));
    echoClient23.SetAttribute("Interval", TimeValue(Seconds(0.001)));
    echoClient23.SetAttribute("PacketSize", UintegerValue(1024));

    ApplicationContainer clientApps23 = echoClient23.Install(nodes23.Get(0));
    clientApps23.Start(Seconds(2.0));
    clientApps23.Stop(Seconds(10.0));

    Time simulationEndTime = Seconds(10.0);

    // Run simulation for 10 seconds
    Simulator::Stop(simulationEndTime);

    Ptr<FlowMonitor> monitor;

    FlowMonitorHelper flowmon; 

    monitor = flowmon.InstallAll();
    
    Simulator::Run();

    NS_LOG_INFO ("Run Simulation.");


    monitor->CheckForLostPackets ();


    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());

    std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();

    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i){

        Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);

        std::cout << "Flow " << i->first  << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";

        std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";

        std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";

        std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / (i->second.timeLastRxPacket.GetSeconds() - i->second.timeFirstTxPacket.GetSeconds())/1024/1024  << " Mbps\n";
    }

    Simulator::Destroy ();

    NS_LOG_INFO ("Done.");

}

